package com.example.student1.users.utils;

/**
 * Created by student1 on 17.04.17.
 */
public class UserResponce {
    private int id;
    private String username;
    private String email;
    private String phone;
    private String website;
    private CompanyResponse company;

    public String getEmail() {
        return email;
    }
    public void setCompany(CompanyResponse company) {
        this.company = company;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public CompanyResponse getCompany() {
        return company;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }


    class CompanyResponse {
        private String name;
        private String catchPhrase;
        private String bs;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCatchPhrase() {
            return catchPhrase;
        }

        public void setCatchPhrase(String catchPhrase) {
            this.catchPhrase = catchPhrase;
        }

        public String getBs() {
            return bs;
        }

        public void setBs(String bs) {
            this.bs = bs;
        }
    }






}
