package com.example.student1.users;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.example.student1.users.models.UsersViewModel;
import com.example.student1.users.utils.UserAdapter;
import com.example.student1.users.utils.UserResponce;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private static String PREDICTOR_URI_JSON = "https://jsonplaceholder.typicode.com";
    private RecyclerView recyclerView;
    private UserAdapter adapter;
    private ArrayList<UsersViewModel> models = new ArrayList<>();

    private Retrofit retrofit;
    private UserAPI UserAPI;
    private TextView username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.albums);

        adapter = new UserAdapter(this);


        adapter.setData(models);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        retrofit = new Retrofit.Builder()
                .baseUrl(PREDICTOR_URI_JSON)
                .addConverterFactory(GsonConverterFactory.create() )
                .build();
        UserAPI  = retrofit.create(UserAPI.class);
        username = (TextView) findViewById(R.id.tv_title);
        Call<List<UserResponce>> response1 = UserAPI.getUsers();
        Call<List<UserResponce>> response2 = UserAPI.getAlbums();
        response1.enqueue(new Callback<List<UserResponce>>() {


            @Override
            public void onResponse(Call<List<UserResponce>> call, Response<List<UserResponce>> response) {
                 String userName = response.body().get(0).getUsername();
                 adapter.setUsername(userName);

            }

            @Override
            public void onFailure(Call<List<UserResponce>> call, Throwable t) {
                Log.d("MYTAG", "error");
            }
        });
        response1.enqueue(new Callback<List<UserResponce>>() {

            @Override
            public void onResponse(Call<List<UserResponce>> call, Response<List<UserResponce>> response) {
                //String albumName = response.body().get(0).
                //adapter.set(albumName);
            }

            @Override
            public void onFailure(Call<List<UserResponce>> call, Throwable t) {

            }
        }
    }

}
