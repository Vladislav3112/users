package com.example.student1.users.utils;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.student1.users.R;
import com.example.student1.users.models.UsersViewModel;

import java.util.ArrayList;

/**
 * Created by student1 on 17.04.17.
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {

    private ArrayList<UsersViewModel> mDataList = new ArrayList<>();
    private Context mContext;
    private String mUsername = "";

    public UserAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_item, parent, false);
        UserViewHolder holder = new UserViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        //UsersViewModel model = mDataList.get(position);
        holder.mTVTitle.setText(mUsername);

    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public void setUsername(String username){
        mUsername = username;
        notifyDataSetChanged();
    }

    public void setData(ArrayList<UsersViewModel> data) {
        mDataList = data;
    }

    class UserViewHolder extends RecyclerView.ViewHolder {

        private ImageView mIVUser;
        private TextView mTVTitle,mTVName,mTVusername;



        public UserViewHolder(View view) {
            super(view);
            mIVUser = (ImageView) view.findViewById(R.id.tv_avatar);
            mTVTitle = (TextView) view.findViewById(R.id.tv_album);
            //mTVName = (TextView) view.findViewById(R.id.tv_name);
        }
    }
}