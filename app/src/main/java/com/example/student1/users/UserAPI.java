package com.example.student1.users;

import com.example.student1.users.models.UsersViewModel;
import com.example.student1.users.utils.UserResponce;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by student1 on 17.04.17.
 */
public interface UserAPI {

    @GET("users")
    Call<List<UserResponce>> getUsers();
    @GET("albums?userId=1")
    Call <List<UserResponce>> getAlbums();

}
