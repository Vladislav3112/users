package com.example.student1.users.models;

/**
 * Created by student1 on 17.04.17.
 */
public class UsersViewModel {

    private String description;
    private String title;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }






}
